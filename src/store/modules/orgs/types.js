'use strict';

export const GET_ORGS_ERROR = 'GET_ORGS_ERROR'
export const GET_ORGS = 'GET_ORGS'
export const GET_ORGS_SUCCESS = 'GET_ORGS_SUCCESS'
