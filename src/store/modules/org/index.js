import * as type from './types'
import actions from './actions'

const state = {
	name: 'Hoo',
	active: true
}

const mutations = {
	[type.GET_ORG](state, action) {
		if(action.org !== null){
			const storedOrg = JSON.parse(action.org);
			Object.assign(state, storedOrg)
		}
	},

	[type.UPDATE_ORG](state, action) {
		Object.assign(state, action.org)
	}
}

export default {
	state,
	mutations,
	actions
}
