'use strict'

export const LOCAL_USER = 'vue.user'
export const LOCAL_ORG = 'vue.org'
export const ORGS_LIST = [
    {
        "id": "org-1",
        "name": "Brrng",
        "active": true
    },
    {
        "id": "org-2",
        "name": "Safaricom",
        "active": true
    },
    {
        "id": "org-3",
        "name": "Twitter",
        "active": true
    },
]