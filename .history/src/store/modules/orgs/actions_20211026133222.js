'use strict';

import Vue from 'vue'
import VueResource from 'vue-resource'
import { SHOW_LOADER, HIDE_LOADER } from '../loader/types'
import * as type from './types'
// const actions = {
// 	getOrgs() {
// 		commit(type.REQUEST_USERS_SUCCESS, {
// 		orgs = JSON.parse(localStorage.getItem(LOCAL_ORG))
// 		})
// 			}
// 		}

const actions = {
	getOrgs({ commit }) {
		commit(type.GET_ORGS)
		commit(SHOW_LOADER, {
			message: 'Fetching orgs...'
		})

		try{
				commit(type.GET_ORGS_SUCCESS, {
					payload: resp.body
				})

				commit(HIDE_LOADER)
			}
			catch(error ){
				commit(type.REQUEST_ORGS_ERROR, {
					error
				})

				commit(SHOW_LOADER, {
					message: 'There was an error fetching the orgs'
				})
			}
	}
}

export default actions;
