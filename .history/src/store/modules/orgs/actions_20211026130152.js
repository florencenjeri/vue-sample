'use strict';

import Vue from 'vue'
import VueResource from 'vue-resource'
import { SHOW_LOADER, HIDE_LOADER } from '../loader/types'
import * as type from './types'
const actions = {
	getOrgs() {
		
		return [
			{
				"id": "org-1",
				"name": "Brrng",
				"active": true
			},
			{
				"id": "org-2",
				"name": "Safaricom",
				"active": true
			},
			{
				"id": "org-3",
				"name": "Twitter",
				"active": true
			},
		]
		
			}
		}

export default actions;
