'use strict';

import * as type from './types'
import actions from './actions'
import { ORGS_LIST } from '../../../constants';

const state = {
	data: []
}

const mutations = {

	[type.GET_ORGS](state) {
		state.data = ORGS_LIST
	}
}

export default {
	state,
	mutations,
	actions
}
