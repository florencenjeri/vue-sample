'use strict';

import Vue from 'vue'
import VueResource from 'vue-resource'
import { SHOW_LOADER, HIDE_LOADER } from '../loader/types'
import * as type from './types'

const actions = {
	getOrgs({ commit }) {
		commit(type.REQUEST_ORGS)
		commit(SHOW_LOADER, {
			message: 'Fetching orgs...'
		})

		Vue.http.get(`https://run.mocky.io/v3/9c3aef7c-f158-4f54-8331-81e2a92f178a`)
			.then(resp => {
				commit(type.REQUEST_ORGS_SUCCESS, {
					payload: resp.body.results
				})

				commit(HIDE_LOADER)
			})
			.catch(error => {
				commit(type.REQUEST_ORGS_ERROR, {
					error
				})

				commit(SHOW_LOADER, {
					message: 'There was an error fetching the orgs'
				})
			})
	}
}

export default actions;
