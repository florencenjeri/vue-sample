'use strict';

import Vue from 'vue'
import VueResource from 'vue-resource'
import { LOCAL_ORG, ORGS_LIST } from '../../../constants';
import { SHOW_LOADER, HIDE_LOADER } from '../loader/types'
import * as type from './types'

const actions = {
	getOrgs({ commit }) {
		commit(type.GET_ORGS)
		commit(SHOW_LOADER, {
			message: 'Fetching orgs...'
		})
		console.log(JSON.parse(localStorage.getItem(LOCAL_ORG)))
		try{
				commit(type.GET_ORGS_SUCCESS, {
					payload: JSON.parse(localStorage.getItem(LOCAL_ORG))
				})

				commit(HIDE_LOADER)
			}
			catch(error ){
				commit(type.GET_ORGS_ERROR, {
					error
				})

				commit(SHOW_LOADER, {
					message: 'There was an error fetching the orgs'
				})
			}
	}
}

export default actions;
