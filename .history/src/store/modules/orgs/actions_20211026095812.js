'use strict';

import Vue from 'vue'
import VueResource from 'vue-resource'
import { SHOW_LOADER, HIDE_LOADER } from '../loader/types'
import * as type from './types'

const actions = {
	getOrgs({ commit }) {
		commit(type.REQUEST_ORGS)
		commit(SHOW_LOADER, {
			message: 'Fetching orgs...'
		})

		Vue.http.get(`https://run.mocky.io/v3/c992173e-5db0-40c2-89ac-eb9a26bd0105`)
			.then(resp => {
				commit(type.REQUEST_ORGS_SUCCESS, {
					payload: resp.body
				})

				commit(HIDE_LOADER)
			})
			.catch(error => {
				commit(type.REQUEST_ORGS_ERROR, {
					error
				})

				commit(SHOW_LOADER, {
					message: 'There was an error fetching the orgs'
				})
			})
	}
}

export default actions;
