'use strict';

const state = {
	orgs: []
}
const actions = {
	getOrgs() {
		this.orgs = JSON.parse(localStorage.getItem(LOCAL_ORG))
			}
		}

export default actions;
