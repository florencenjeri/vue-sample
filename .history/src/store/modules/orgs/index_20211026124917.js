'use strict';

import * as type from './types'
import actions from './actions'
import { ORGS_LIST } from '../../../constants';

const state = {
	data: []
}

const mutations = {
	[type.REQUEST_ORGS](state) {
		state.isFetching = true
		state.error = null
	},

	[type.REQUEST_ORGS_SUCCESS](state, action) {
		state.isFetching = false
		state.data = action.payload
		state.error = null
	},

	[type.REQUEST_ORGS_ERROR](state, action) {
		state.isFetching = false
		state.error = action.error
	}
}

export default {
	state,
	mutations,
	actions
}
