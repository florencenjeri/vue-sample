import { LOCAL_ORG } from "../../../constants";
import * as type from './types';

function getLocalOrg(){
	return localStorage.getItem(LOCAL_ORG);
}

const actions = {
	getOrg({ commit }){
		const storedOrg = getLocalOrg() 
		commit(type.GET_ORG, {
			org: storedOrg
			
		})
	},
	updateOrg({ commit }, org){
		var orgs = []
		orgs.push(org)
		console.log(orgs)
		localStorage.setItem(LOCAL_ORG, JSON.stringify(orgs));

		commit(type.UPDATE_ORG, {
			org
		})
		
	}
}

export default actions;