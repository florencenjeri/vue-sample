import { LOCAL_ORG } from "../../../constants";
import * as type from './types';

function getLocalOrg(){
	return localStorage.getItem(LOCAL_ORG);
}

const actions = {
	getOrg({ commit }){
		const storedOrg = getLocalOrg() 
		commit(type.GET_ORG, {
			org: storedOrg
			
		})
	},
	updateOrg({ commit }, org){
		localStorage.setItem(LOCAL_ORG, JSON.stringify(org));
		console.log(JSON.parse(localStorage.getItem(ORGS_LIST)))

		commit(type.UPDATE_ORG, {
			org
		})
	}
}

export default actions;