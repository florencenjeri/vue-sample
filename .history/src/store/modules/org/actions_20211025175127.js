import { LOCAL_ORG } from "../../../constants";
import * as type from './types';

function getLocalUser(){
	return localStorage.getItem(LOCAL_ORG);
}

const actions = {
	getUser({ commit }){
		const storedOrg = getLocalUser()

		commit(type.GET_ORG, {
			user: storedOrg
		})
	},

	updateUser({ commit }, user){
		localStorage.setItem(LOCAL_USER, JSON.stringify(user));

		commit(type.UPDATE_USER, {
			user
		})
	}
}

export default actions;