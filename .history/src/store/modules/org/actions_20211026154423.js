import { LOCAL_ORG } from "../../../constants";
import * as type from './types';

function getLocalOrg(){
	return localStorage.getItem(LOCAL_ORG);
}

const actions = {
	getOrg({ commit }){
		const storedOrg = getLocalOrg() 
		commit(type.GET_ORG, {
			org: storedOrg
			
		})
	},
	updateOrg({ commit }, org){
		var localOrg = localStorage.getItem(LOCAL_ORG);
			localOrg = localOrg.split('}')
			localOrg.push(org)
		
		localStorage.setItem(LOCAL_ORG, JSON.stringify(localOrg));

		commit(type.UPDATE_ORG, {
			org
		})
		
	}
}

export default actions;